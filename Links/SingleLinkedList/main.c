//
//  main.c
//  cmdTest
//
//  Created by Woong.H on 2015. 4. 28..
//  Copyright (c) 2015년 Woong.H. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "main.h"

stList *nodePtr;

stList* create_node_batteryLvl(uint8_t numNode)
{
  stList *newNode;
  newNode = (stList*)malloc(sizeof(stList));
  
  if (numNode > MAX_SAMPLE_LENTH)
  {
    printf("Too many nodes!\n");
    return NULL;
  }
  
  if (numNode == NUM_OF_SAMPLES)
  {
    nodePtr = newNode;
  }
  
  newNode->value = numNode;
  
  numNode--;
  if (numNode > 0)
  {
    newNode->next = create_node_batteryLvl(numNode);
  }
  else if (numNode == 0)  // Last node
  {
    newNode->next = nodePtr;
  }

  //if error return -1;
  return newNode;
}

uint8_t Update_batteryLvl_node(uint16_t batteryLvl)
{

  nodePtr->value = batteryLvl;
  nodePtr = nodePtr->next;
  return 0;
}

uint16_t get_max_batteryLvl(void)
{
  uint16_t max=0;
  uint8_t cnt;
  stList *ptr;
  
  ptr = nodePtr;
  
  for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
  {
    if(max < ptr->value)
      max = ptr->value;
    ptr = ptr->next;
  }
  
  return max;
}

uint16_t get_min_batteryLvl(void)
{
  uint16_t min=0xFFFF;
  uint8_t cnt;
  stList *ptr;
  
  ptr = nodePtr;
  
  for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
  {
    if(min > ptr->value)
      min = ptr->value;
    ptr = ptr->next;
  }
  
  return min;
}


uint16_t Get_mean_batteryLvl(void)
{
  uint16_t mean=0;
  uint8_t cnt;
  stList *ptr;
  
  ptr = nodePtr;
  
  for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
  {
    mean += ptr->value;
    ptr = ptr->next;
  }
  mean = mean - (get_max_batteryLvl()+get_min_batteryLvl());
  mean = mean/(NUM_OF_SAMPLES-2);
  return mean;
}

int main(int argc, const char * argv[]) {
  uint8_t cnt;
  stList *ptr;
  
  ptr = create_node_batteryLvl(NUM_OF_SAMPLES);
  
  printf("\nNode val: ");
  for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
  {
    printf("%d ", ptr->value);
    ptr = ptr->next;
  }
  
  Update_batteryLvl_node(15);
  Update_batteryLvl_node(9000);
  Update_batteryLvl_node(0);

  printf("\nNode val: ");
  for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
  {
    printf("%d ", ptr->value);
    ptr = ptr->next;
  }

  printf("\n");
  printf("MAX: %d\n", get_max_batteryLvl());
  printf("MIN: %d\n", get_min_batteryLvl());
  printf("MEAN: %d\n", Get_mean_batteryLvl());
  printf("\n");
  
  return 0;
}
