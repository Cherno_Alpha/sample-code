//
//  main.h
//  cmdTest
//
//  Created by Woong.H on 2015. 4. 28..
//  Copyright (c) 2015년 Woong.H. All rights reserved.
//

#ifndef cmdTest_main_h
#define cmdTest_main_h


#endif

#define U8 unsigned char

#define NUM_OF_SAMPLES   10
#define MAX_SAMPLE_LENTH 100
#define DEFAULT_VALUE    0

typedef struct _sampleList
{
  uint16_t value;
  struct _sampleList *next;
}stList;
//typedef struct _sampleList stList;

stList* create_node_batteryLvl(uint8_t);

uint8_t Update_batteryLvl_node(uint16_t);
uint16_t get_max_batteryLvl(void);
uint16_t get_min_batteryLvl(void);
uint16_t Get_mean_batteryLvl(void);