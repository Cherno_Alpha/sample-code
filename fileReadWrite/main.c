/*
입력: 파일명 brayden 의 바이너리 파일.
출력: 파일명 brayden.txt 의 텍스트 파일

동작: 2byte 를 읽어 하위 12bits는 데이터로, 상위 4bits는 코드로 분리하여 파일에 저장한다.
예: 227D 을 읽어 637, 2 로 저장. Mac
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum
{
	NONE,
	MAX,
	MIN
};

int main( void)
{
	FILE	*fp_sour;
	FILE	*fp_dest;
	char	buf[2];
	char	tag;
	char	num[20];
	
	fp_sour = fopen( "./brayden", "rb");
	if (fp_sour!=NULL)
	{
		fp_dest = fopen( "./brayden.txt", "w");
		if (fp_dest)
		{
			while( (EOF != (buf[0] = fgetc(fp_sour))) &&
					(EOF != (buf[1] = fgetc(fp_sour))) )
			{
            	unsigned short depth;
            	depth = ((buf[0]&0x0F)<<8) | buf[1];

            	snprintf(num, 20, "%d, %d\n", depth, (unsigned int)((buf[0]&0xF0)>>4));
            	printf("%s", num);
            	fputs(num, fp_dest);
			}
			
			fclose( fp_dest);
		}
		else
		{
			printf("Destination file open error\n");
		}
		fclose( fp_sour);
	}
	else
	{
		printf("Source file open error\n");
	}
	
	return 0;
}
