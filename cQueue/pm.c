#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "pm.h"

stListQ battQ;

bool Create_battLvlQueue(uint8_t qlength)
{
	battQ.size = qlength;
	battQ.ptr = Create_node_queue(battQ.size);

	if(battQ.ptr == NULL)
		return false;
	else
		return true;
}

uint16_t scan_battADC(void)
{
	// Do something
	return 32;
}

void put_battLvl_on_Queue(uint16_t battLvl)
{
	Update_data_queue(battQ.ptr, battLvl);
	battQ.ptr = battQ.ptr->next;
}

bool Update_battLvlQueue(void)
{
	uint16_t battADC;
	battADC = scan_battADC();
	put_battLvl_on_Queue(battADC);

	return true;
}

uint16_t get_max_battLvlQueue(void)
{
	return get_max_node(battQ.ptr);
}

uint16_t get_min_battLvlQueue(void)
{
	return get_min_node(battQ.ptr);
}

uint16_t get_average_battLvl_on_Queue(void)
{
	return Get_average_node(battQ.ptr, NUM_OF_BATT_SAMPLES);
}

bool Update_battStatus(void)
{
	return true;
}

#ifdef module_test_power_management
void TEST_PM_MODULE(void)
{
	uint16_t cnt;

	printf("\n2nd Node val: ");
	for (cnt=0 ; cnt<NUM_OF_BATT_SAMPLES ; cnt++)
	{
		printf("%d ", battQ.ptr->value);
		battQ.ptr = battQ.ptr->next;
	}

	put_battLvl_on_Queue(15);
	put_battLvl_on_Queue(16000);
	put_battLvl_on_Queue(150);
	put_battLvl_on_Queue(110);

	printf("\n2nd Node val: ");
	for (cnt=0 ; cnt<NUM_OF_BATT_SAMPLES ; cnt++)
	{
		printf("%d ", battQ.ptr->value);
		battQ.ptr = battQ.ptr->next;
	}

	printf("\n");
	printf("MAX: %d\n", get_max_battLvlQueue());
	printf("MIN: %d\n", get_min_battLvlQueue());
	printf("MEAN: %d\n", get_average_battLvl_on_Queue());
	printf("\n");

}
#endif
