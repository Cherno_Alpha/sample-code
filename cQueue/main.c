//
//  main.c
//  cmdTest
//
//  Created by Woong.H on 2015. 4. 28..
//  Copyright (c) 2015년 Woong.H. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"
#include "pm.h"



int main(int argc, const char * argv[]) {
	uint8_t cnt;
	stList *ptr;
	
	ptr = Create_node_queue(NUM_OF_SAMPLES);
	
	printf("\nNode val: ");
	for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
	{
		printf("%d ", ptr->value);
		ptr = ptr->next;
	}
	
	Update_batteryLvl_node(15);
	Update_batteryLvl_node(9000);
	Update_batteryLvl_node(0);

	printf("\nNode val: ");
	for (cnt=0 ; cnt<NUM_OF_SAMPLES ; cnt++)
	{
		printf("%d ", ptr->value);
		ptr = ptr->next;
	}

	printf("\n");
	printf("MAX: %d\n", get_max_batteryLvl());
	printf("MIN: %d\n", get_min_batteryLvl());
	printf("MEAN: %d\n", Get_mean_batteryLvl());
	printf("\n");
	
	Create_battLvlQueue(NUM_OF_BATT_SAMPLES);
	
	#ifdef module_test_power_management
	TEST_PM_MODULE();
	#endif

	return 0;
}
