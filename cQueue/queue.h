//
//  main.h
//  cmdTest
//
//  Created by Woong.H on 2015. 4. 28..
//  Copyright (c) 2015년 Woong.H. All rights reserved.
//
#include <stdbool.h>
#include <stdint.h>

#ifndef cmdTest_main_h
#define cmdTest_main_h

#endif

#define U8 unsigned char

#define NUM_OF_SAMPLES   10
#define MAX_SAMPLE_LENTH 100
#define DEFAULT_VALUE    0

typedef struct _sList
{
	uint16_t value;
	struct _sList *next;
}stList;

typedef struct _sListQ
{
	stList *ptr;
	uint8_t size;
} stListQ;

stList* Create_node_queue(uint8_t);

void Update_data_queue(stList *, uint16_t);
uint16_t get_max_node(stList *);
uint16_t get_min_node(stList *);
uint16_t Get_average_node(stList *, uint8_t);

uint8_t Update_batteryLvl_node(uint16_t);
uint16_t get_max_batteryLvl(void);
uint16_t get_min_batteryLvl(void);
uint16_t Get_mean_batteryLvl(void);
