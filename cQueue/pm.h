// Header file for Power management

#include <stdbool.h>
#include <stdint.h>

#define module_test_power_management

#define NUM_OF_BATT_SAMPLES	10

bool Create_battLvlQueue(uint8_t qLength);
bool Update_battLvlQueue(void);
uint16_t scan_battADC(void);
void put_battLvl_on_Queue(uint16_t battLvl);
bool Update_battStatus(void);
uint16_t get_max_battLvlQueue(void);
uint16_t get_min_battLvlQueue(void);
uint16_t get_average_battLvl_on_Queue(void);
void setFlag_battStatus(uint16_t battLvl);
void PowerOn_indicate(void);
void setFlag_welcomeLED(void);
void indicate_FirmwareVer(void);
void blinking_pwrLED(void);
bool Indicate_battStatus(void);
void indicate_pwrLED_green(void);
void indicate_pwrLED_greenBlink(void);
void indicate_pwrLED_redBlink(void);

#ifdef module_test_power_management
void TEST_PM_MODULE(void);
#endif